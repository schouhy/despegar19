FROM python:3

ADD src /src

ADD models/model.pkl /models/model.pkl

ADD setup.py /

ADD predict.py /

ADD requirements.txt /

RUN pip install -r requirements.txt

CMD [ "python", "./predict.py" ]

