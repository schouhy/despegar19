ECI 2019 Despegar
==============================

Con Makefile
---

`conda create --name despegar python=3`

`conda activate despegar`

`make predict IMG_PATH=path/to/evaluacion_final`

Los resultados están en el archivo solucion.csv

Con Dockerfile
---

Crear la imagen

`docker build -t predict .`

Ejecutar el script (solo reemplazar "path/to/evaluacion_final" por lo que 
corresponda)

`docker run --shm-size=1g --mount source=path/to/evaluacion_final,target=/data/raw/test,type=bind predict`

Sacar el container_id.

`docker ps -a`
 
Copiar la solución del container al host.
 
`docker cp container_id:/solucion.csv ./solucion.csv`

