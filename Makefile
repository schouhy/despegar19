.PHONY: requirements predict

## Install Python Dependencies
requirements:
	python3 -m pip install -U pip setuptools wheel
	python3 -m pip install -r requirements.txt

predict: requirements
	rm -f data/raw/test
	mkdir -p data/raw
	ln -s $(IMG_PATH) data/raw/test
	python3 predict.py
