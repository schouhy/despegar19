from src.nb_06 import *

data = ImageList.from_folder('data/raw/test')
learn = load_learner('models', 'model.pkl', test=data)


preds = learn.TTA(ds_type=DatasetType.Test)
preds = preds[0].data.cpu().numpy()
fnames = [str(p) for p in learn.data.test_ds.items]


solucion = pd.DataFrame({'id': fnames, 'target': preds.argmax(1)})
solucion['id'] = solucion['id'].str.extract('([0-9]+)').astype(int)
solucion = solucion.sort_values('id')


solucion.to_csv('solucion.csv', index=False, header=False)

